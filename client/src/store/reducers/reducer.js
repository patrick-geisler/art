const initialState = {
  artData: [],
  isDataFetching: false,
  dataFetched: false,
  searchText: ''
}

const getArt = (state = initialState, action) => {
  switch (action.type) {
    case `ADD_ART`:
      return Object.assign({}, state, {
        artData: action.data
      })
    case `DATA_FETCHED`:
      return Object.assign({}, state, {
        dataFetched: action.bool
      })
    case `IS_DATA_FETCHING`:
      return Object.assign({}, state, {
        isDataFetching: action.bool
      })
    case `SEARCH_TEXT`:
      return Object.assign({}, state, {
        searchText: action.text
      })
    default:
      return state
  }
}

export default getArt
