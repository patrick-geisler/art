const addArt = (data) => {
  return {
    type: `ADD_ART`,
    data
  }
}

const isDataFetching = (bool) => {
  return {
    type: `IS_DATA_FETCHING`,
    bool
  }
}

const dataFetched = (bool) => {
  return {
    type: `DATA_FETCHED`,
    bool
  }
}

const searchText = (text) => {
  return {
    type: `SEARCH_TEXT`,
    text
  }
}
export {
  addArt,
  isDataFetching,
  dataFetched,
  searchText
}
