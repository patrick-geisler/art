// https://github.com/reduxjs/reselect#createselectorinputselectors--inputselectors-resultfunc
import {createSelector} from 'reselect'

const getFilteredSearch = (state) => {
  const { searchText, artData } = state
  return artData.filter((artItem) => {
    if (artItem.title.toLowerCase().indexOf(searchText.toLowerCase()) !== -1) {
      return true
    }
  })
}

export const filteredArt = createSelector(
  getFilteredSearch,
  (getFilteredSearchSelector) => getFilteredSearchSelector
)

export default getFilteredSearch
