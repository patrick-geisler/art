import React from 'react'
import {TextField, DropDownMenu, MenuItem} from 'material-ui'
import '../App.css'
import { connect } from 'react-redux'
import {searchText} from '../store/actions/actions'

const Search = (props) => {
  const { handleTextChange, searchText } = props
  console.log(props)
  return (
    <div className='search'>
      <TextField
        hintText={`Search By ${`Title`}`}
        onChange={handleTextChange}
        value={searchText}
      />
      {/* <DropDownMenu
          value={this.state.value}
          onChange={this.handleMenuChange}
        >
          <MenuItem value={0} primaryText='Title' />
          <MenuItem value={1} primaryText='Artist' />
          <MenuItem value={2} primaryText='Date' />
        </DropDownMenu> */}
    </div>
  )
}

const mapStateToProps = state => {
  return {
    searchText: state.searchText
  }
}

const mapDispatchToProps = dispatch => {
  return {
    handleTextChange: e => {
      dispatch(searchText(e.target.value))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search)
