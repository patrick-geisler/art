import React from 'react'
import ImageTemplate from './ImageTemplate'
import { connect } from 'react-redux'
import getFilteredArt from '../store/selectors/getFilteredArt'
import '../App.css'

const mapStateToProps = state => {
  return {
    artData: getFilteredArt(state)
  }
}

function ImageCard ({state, artData}) {
  return (
    <div>
      Test
      <div className='imageContainer'>
        {artData.map((title, index) => {
          return (
            <div key={index}>
              <ImageTemplate title={title.title} url={title.url} index={index} />
            </div>
          )
        })
        }
      </div>
    </div>
  )
}

export default connect(
  mapStateToProps
)(ImageCard)
