import React, { Component } from 'react'
import '../App.css'
import Index from 'patricks-react-hello-world'

class ImageTemplate extends Component {
  render () {
    const { title, url, index } = this.props
    return (
      <div className='images'>
        <img src={url} alt={index} height={300} width={300}/>
        <div key={index}>
          {title}
          <Index />
        </div>
      </div>
    )
  }
}

export default ImageTemplate
