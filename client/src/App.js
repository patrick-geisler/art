/* global fetch */
import React, { Component } from 'react'
import './App.css'
import Search from './components/Search'
import ImageCard from './components/ImageCard'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducer from './store/reducers/reducer'
import {addArt, isDataFetching, dataFetched} from './store/actions/actions'

const store = createStore(reducer)
const boundAddArt = text => store.dispatch(addArt(text))
const boundIsDataFetching = text => store.dispatch(isDataFetching(text))
const boundDataFetched = text => store.dispatch(dataFetched(text))

class App extends Component {
  componentDidMount () {
    boundDataFetched(false)
    boundIsDataFetching(true)
    fetch(`http://localhost:9000/`)
      .then(res => res.json())
      .then(res => {
        let titles = res.map(item => {
          let obj = {
            title: item.title,
            url: item.url,
            artist: item.artist
          }
          return obj
        })
        return titles
      })
      .then(obj => {
        boundAddArt(obj)
      })
      .then(() => {
        boundIsDataFetching(false)
        boundDataFetched(true)
        console.log(store.getState())
      })
  }

  render () {
    return (
      <Provider store={store}>
        <MuiThemeProvider>
          <div className='App'>
            <h1>This is Art</h1>
            <Search />
            <ImageCard />
          </div>
        </MuiThemeProvider>
      </Provider>
    )
  }
}

export default App
