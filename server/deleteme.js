function firstNotRepeatingCharacter (s) {
  let trackSet = new Set()
  let first = []
  let firstCounter
  for (let i = 0; i < s.length; i++) {
    if (!trackSet.has(s[i])) {
      trackSet.add(s[i])
      first[s[i]] = s[i]
    } else {
      first[s[i]] = null
    }
  }
  return first ? first[0] : '_'
}

console.log(firstNotRepeatingCharacter('aaaaaaacbbbbfbbbb'))

// Functional > OOP
// Prototypical > Classical
// one way data binding in react
