const express = require('express')
const app = express()
const fetch = require('node-fetch')
const https = require('https')

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

const agent = new https.Agent({
  rejectUnauthorized: false
})

app.get('/', function (req, res) {
  let fetchUrl = `https://www.rijksmuseum.nl/api/en/collection?key=maeAVeUh&format=json&ps=500`
  let userSearch = req.query.maker ? `&maker=${req.query.maker}` : ``
  console.log(`fetch URL == ${fetchUrl}${userSearch}`)

  fetch(`${fetchUrl}`, {
    agent: agent
  })
    .then(res => res.json())
    .then(json => {
      const data = json.artObjects.map(obj => (
        {
          title: obj.title,
          url: obj.webImage.url,
          artist: obj.principalOrFirstMaker
        }
      ))
      return data
    })
    .then(data => {
      if (req.query.maker) {
        let artistFilter = data.filter(obj => (
          obj.artist.startsWith(req.query.maker)
        ))
        return artistFilter
      } else {
        return data
      }
    })
    .then(data => res.send(data))
    .catch(error => console.log(error))
})

app.listen(9000, function () {
  console.log('App running on 9000')
})
